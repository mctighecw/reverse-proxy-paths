# README

This is an example of a reverse proxy using Docker, NGINX, HTML, and a couple of boilerplate React apps.

The goal was to improve my understanding of using NGINX as a reverse proxy.

## App Information

App Name: reverse-proxy-paths

Created: May 2018

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/reverse-proxy-paths)

## Notes

* The reverse proxy app runs on port 80.

* There are the following paths:
  - `/` (home)
  - `/site1`
  - `/site2`
  - `/site3`
  - `/nginx`
  - `/apache`

## To Run

```
$ source build.sh
```

Last updated: 2025-02-13
